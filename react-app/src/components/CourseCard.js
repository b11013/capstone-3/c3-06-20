import { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import { isDOMComponent } from "react-dom/test-utils";

export default function CourseCard({ courseProp }) {
  console.log(courseProp);

  const { name, description, price } = courseProp;

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);
  const [isOpen, setIsOpen] = useState(false);

  //*useState returns an array with two elements
  const enroll = () => {
    setCount(count + 1);
    console.log(`Enrollees ${count}`);

    if (seats > 0) {
      setCount(count + 1);
      console.log("Enrollees: " + count);
      setSeats(seats - 1);
      console.log("Seats: " + seats);
    } else {
      alert("No more seats available");
    }
  };

  useEffect(() => {
    if (seats === 0) {
      setIsOpen(true);
    }
  }, [seats]);

  return (
    <Card className="mb-4">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text> Seats: {seats}</Card.Text>
        <Button variant="primary" onClick={enroll} disabled={isOpen}>
          Enroll
        </Button>
      </Card.Body>
    </Card>
  );
}
