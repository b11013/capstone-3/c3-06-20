import React from "react";
import "./Banner.css";

function Banner() {
  return (
    <div className="banner">
      <div className="banner__container">
        <img
          className="banner__image"
          src="https://m.media-amazon.com/images/I/61-8rBAD68L._SX3000_.jpg"
          alt=""
        />
      </div>
    </div>
  );
}

export default Banner;
