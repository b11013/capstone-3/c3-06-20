import React from "react";
import "./AppNavBar.css";
import { BsSearch } from "react-icons/bs";
import { MdShoppingBasket } from "react-icons/md";
import { Link } from "react-router-dom";
import { useStateValue } from "./StateProvider";

function AppNavBar() {
  const [{ basket }, dispatch] = useStateValue();

  return (
    <div className="header">
      <Link as={Link} to="/">
        <img
          className="header__logo"
          src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
          alt="logo"
        />
      </Link>

      <div className="header__search">
        <input className="header__searchInput" type="text" />
        <BsSearch className="header__searchIcon" />
      </div>

      <nav className="header__nav">
        <div className="header__option">
          <span className="header__optionLineOne">Hello, Guest</span>
          <Link as={Link} to="/login">
            <span className="header__optionLineTwo">Sign In</span>
          </Link>
        </div>

        <div className="header__option">
          <span className="header__optionLineOne">Returns</span>
          <span className="header__optionLineTwo">& Orders</span>
        </div>

        <div className="header__optionBasket">
          <Link as={Link} to="/checkout">
            <MdShoppingBasket />
          </Link>
          <span className="header__optionLineTwo header__basketCount">
            {basket?.length}
          </span>
        </div>
      </nav>
    </div>
  );
}

export default AppNavBar;
