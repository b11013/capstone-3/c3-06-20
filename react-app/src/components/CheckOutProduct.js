import React from "react";
import { Row, Col, Card, Button } from "react-bootstrap";
import "./Banner.css";
import "./Featured.css";
import { useStateValue } from "./StateProvider";

function CheckOutProduct({ id, title, image, price, rating }) {
  const [{ basket }, dispatch] = useStateValue();

  console.log("This is the basket >>> ", basket);

  const addToBasket = () => {
    //dispatch the item into the data layer
    dispatch({
      type: "ADD_TO_BASKET",
      item: {
        id: id,
        title: title,
        image: image,
        price: price,
        rating: rating,
      },
    });
  };

  return (
    <div className="featured">
      <Row className="m-5">
        <Col xs={12} md={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/398409236.png"
                alt="{Image}"
              />
            </Card.Body>

            <button onclick={addToBasket}>Check Out</button>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default CheckOutProduct;
