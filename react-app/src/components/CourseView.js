import { useState, useEffect, useContext } from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function CourseView() {
  const { user } = useContext(UserContext);

  // this is a hook useNavigate to redirect to another page
  const history = useNavigate();

  // retrieves the courseId passed in the URL
  const { courseId } = useParams();

  // we use useState because it depends where you want to select which course to display
  const [name, setName] = useState(""); //starting value
  const [description, setDescription] = useState(""); //then possibly change the value
  const [price, setPrice] = useState(0);

  const enroll = (courseId) => {
    fetch("http://localhost:4000/users/enroll", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        courseId: courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        // to show the alert if the user is enrolled in the course successfully
        if (data) {
          Swal.fire({
            title: "Successfully Enrolled",
            icon: "success",
            text: "Thank you for enrolling!",
          });

          history("/courses"); // rather than using nav component this will be easier using nav needs to import or putting redirection at JSX elements
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again later!",
          });
        }
      });
  };

  useEffect(() => {
    console.log(courseId);

    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setPrice(data.price);
        setDescription(data.description);
      });
  }, [courseId]);

  //* this is a card that will display the course information
  //* this are JSX elements
  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body>
              <Card.Title>{name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>

              <Card.Subtitle>Class Schedule</Card.Subtitle>
              <Card.Text>8 AM to 5 PM</Card.Text>

              {user.id !== null ? (
                <Button variant="primary" onClick={() => enroll(courseId)}>
                  Enroll
                </Button>
              ) : (
                <Link className="btn btn-danger" to="/login">
                  Log In
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
