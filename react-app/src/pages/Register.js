import { Button, Form, Row, Col } from "react-bootstrap";
import "./Register.css";
import { BsExclamation } from "react-icons/bs";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

function RegisterUser() {
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  //check if everything has been filled up
  useEffect(() => {
    if (firstName !== "" && lastName !== "" && email !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email]);

  //verifying
  function registration(e) {
    e.preventDefault();

    if (password.length < 6) {
      Swal.fire({
        title: "Registration failed",
        icon: "error",
        text: "Password must be atleast 6 characters",
      });
    } else {
      Swal.fire({
        title: "Registration successful",
        icon: "success",
        text: "Thank you for registering",
      });
    }
  }

  //hello
  return (
    <div className="background-image">
      <div className="bg-image">
        <Row xs={1} md={2}>
          <Col>
            <img
              className="amazon__logo"
              src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
              alt="logo"
            />
            <p className="text-white">
              Already have an Account? Click <a href="/login"> Here..</a>
            </p>
          </Col>
          <Col>
            <Form
              className="register__form mt-4"
              onSubmit={(e) => registration(e)}
            >
              <h1 className="register__here mb-3">Register Here</h1>
              <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter first name"
                  required
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter last name"
                  required
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="name@example.com"
                  required
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="mobileNumber">
                <Form.Label>Mobile Number:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="+(63)"
                  required
                  value={mobileNumber}
                  onChange={(e) => setMobileNumber(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter your password."
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <BsExclamation />
                <Form.Text className="text-white">
                  Passwords must be at least 6 characters.
                </Form.Text>
              </Form.Group>

              {isActive ? (
                <Button
                  className="submit_button"
                  variant="warning"
                  type="submit"
                >
                  <strong>Submit</strong>
                </Button>
              ) : (
                <Button
                  className="submit_button"
                  variant="warning"
                  type="submit"
                  disabled
                >
                  <strong>Submit</strong>
                </Button>
              )}
            </Form>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default RegisterUser;
