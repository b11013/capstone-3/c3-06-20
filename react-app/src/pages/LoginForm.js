import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "./LoginForm.css";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

function Login() {
  // wala pang pinapasa
  const [email, loginEmail] = useState("");
  const [password, loginPassword] = useState("");
  const [isActive, loginActive] = useState(false);

  //checking if email and pass is not empty
  useEffect(() => {
    if (email !== "" && password !== "") {
      loginActive(true);
    } else {
      loginActive(false);
    }
  }, [email, password]);

  //prevents the refresh
  function loginUser(e) {
    e.preventDefault();

    //swal-trial

    // if(email === true){
    Swal.fire({
      title: "Login Successful",
      icon: "success",
      text: "Welcome!",
    });
    // }else{
    // 	Swal.fire({
    // 			title: 'Authentication Failed',
    // 			icon: 'error',
    // 			text: 'Check your credentials'
    // 			})
    // }

    // loginEmail('');
  }

  return (
    <div className="box-form-main">
      <div className="box-form">
        <Form onSubmit={(e) => loginUser(e)} className="login__form">
          <img
            className="amazon_logo"
            src="http://pngimg.com/uploads/amazon/amazon_PNG11.png"
            alt="logo"
          />
          <h2>Sign-in</h2>

          <Form.Group className="text-field mb-4" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="name@example.com"
              required
              value={email}
              onChange={(e) => loginEmail(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="text-field mb-4" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              required
              value={password}
              onChange={(e) => loginPassword(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mt-3 mb-3" controlId="formBasicPassword">
            {/*disabling button condition*/}
            {isActive ? (
              <Button
                className="login__button text-white"
                variant="warning"
                type="submit"
              >
                Submit
              </Button>
            ) : (
              <Button
                className="login__button text-white"
                variant="warning"
                type="submit"
                id="loginButton"
                disabled
              >
                Login
              </Button>
            )}
          </Form.Group>

          <Form.Text className="text-white">
            New to Amazon? Click <a href="/register">Here..</a>
          </Form.Text>
        </Form>
      </div>
    </div>
  );
}

export default Login;
