//* Context API
// passing information from one companent to another without using props
import React from "react";

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;
export default UserContext;
