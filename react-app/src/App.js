//! Root File
import { Container } from "react-bootstrap";
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import Courses from "./pages/Courses";
import CourseView from "./components/CourseView";
import CourseCard from "./components/CourseCard";
import LoginForm from "./pages/LoginForm";
import Register from "./pages/Register";
import Checkout from "./components/Checkout";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <Router>
      <AppNavBar />

      <Container style={{ maxWidth: "90%" }}>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/login" element={<LoginForm />} />
          <Route exact path="/courses" element={<Courses />} />
          <Route exact path="/coursecard" element={<CourseCard />} />
          <Route exact path="/courseView/:courseId" element={<CourseView />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/checkout" element={<Checkout />} />
        </Routes>
      </Container>
      <Footer />
    </Router>
  );
}

export default App;
