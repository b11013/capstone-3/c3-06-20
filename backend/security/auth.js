const jwt = require('jsonwebtoken');
const secret =
  '3c23825c1beb68cc53f778f3a02b965e26a0c35c40d3ecf55041b0cbde0ed0bcf61773605b0c61ba8570355a21a7bbdd5f9cb5b4900df8b13d7d9b910b3bcb4e';

//generate token
let generateToken = (user) => {
  let data = {
    id: user._id,
    firstName: user.firstName,
    email: user.email,
    isActive: user.isActive,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, { expiresIn: '1hr' });
};

//check token
let verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;

  const token = authHeader.split(' ')[1];
  // console.log(token);

  if (token === 'undefined') return res.send('Token cannot be null');

  jwt.verify(token, secret, (err, decodedToken) => {
    if (err) {
      res.send({
        auth: 'Failed',
        message: err.message,
      });
    } else {
      req.user = decodedToken;
      next();
    }
  });
};

//AISHA
let isAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: 'Failed',
      message: 'Action Forbidden',
    });
  }
};

module.exports = {
  generateToken,
  verifyToken,
  isAdmin,
};
