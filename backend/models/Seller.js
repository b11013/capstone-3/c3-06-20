const mongoose = require('mongoose');

const sellerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Seller name cannot be empty'],
  },
  email: {
    type: String,
    required: [true, 'Email cannot be empty'],
  },
  password: {
    type: String,
    required: [true, 'Password cannot be empty'],
  },
  description: {
    type: String,
    required: [true, 'Seller description cannot be empty'],
  },
  isAdmin: {
    type: Boolean,
    default: true,
  },
  products: [
    {
      productId: String,
    },
  ],
});

module.exports = mongoose.model('Seller', sellerSchema);
