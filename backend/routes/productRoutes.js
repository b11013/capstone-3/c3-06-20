const express = require("express");
const router = express.Router();
const auth = require("../security/auth");
const { verifyToken } = auth;

const productControllers = require("../controllers/productControllers");

router.get("/viewAll", productControllers.viewAllProducts);

router.get("/search", productControllers.findProduct);

router.get("/:id", productControllers.viewProduct);

router.post("/leaveReview/:id", verifyToken, productControllers.leaveReview);

// AISHA
//router.post("/add", verifyToken, isAdmin, productControllers.addProducts);

module.exports = router;
