const express = require('express');
const router = express.Router();
const auth = require('../security/auth');

const { verifyToken } = auth;

const customerControllers = require('../controllers/customerControllers');

//NOTE: Register
router.post('/register', customerControllers.registerCustomer);

//NOTE: Check if email exist
router.post('/checkEmail', customerControllers.checkIfEmailExist);

//NOTE: Login
router.post('/login', customerControllers.login);

// NOTE: Get single user
router.get('/getSingleUser', verifyToken, customerControllers.getSingleUser);

// NOTE: Activate
router.put('/isActive', verifyToken, customerControllers.activateUser);

// NOTE: Deactivate
router.put('/deactivate', verifyToken, customerControllers.deactivateUser);

router.post('/order/:id', verifyToken, customerControllers.orderProduct);

module.exports = router;
