const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const PORT = process.env.PORT || 4000;
const app = express();

mongoose
  .connect(
    "mongodb+srv://admin:admin@capstone3.q5ykl.mongodb.net/e-commerce?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => console.log(`DB Connected`))
  .catch((err) => console.log(`Error in connection ${err}`));

//middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//TODO: Group Routing

const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

const customerRoute = require("./routes/customerRoutes");
app.use("/customer", customerRoute);

const sellerRoutes = require("./routes/sellerRoutes");
app.use("/seller", sellerRoutes);

app.listen(PORT, () => console.log(`Listening to port: ${PORT}`));
