const Product = require('../models/Product');
const Seller = require('../models/Seller');
const bcrypt = require('bcryptjs');

//add
const addProducts = (req, res) => {
  const { name, description, price, quantity, categories } = req.body;

  const addNewProducts = new Product({
    sellerId: req.user.id,
    name,
    description,
    price,
    quantity,
    categories,
  });

  Product.findOne({ name }).then((data) => {
    if (data !== null && data.name === name) {
      return res.send('Product already exists');
    } else {
      addNewProducts
        .save()
        .then(async (data) => {
          //find the seller through the sellerid
          //find the productid based on the _id taken from the data parameter
          let checker = await Seller.findById(data.sellerId).then((result) => {
            /*  Result
            {
                  "_id": "62b01238190de944474fbeba",
                  "name": "Code Zero",
                  "email": "user@test.com",
                  "password": "$2a$10$igxSrTM8x6fLbOzpScL.7O2vMzEqxDAKjV73r/UuCkF5KUfpYaloy",
                  "description": "Your one stop shop for any fizzy drink.",
                  "isAdmin": true,
                  "products": [],
                  "__v": 0
              }
             */

            /*DATA
            {
                "sellerId": "62b01238190de944474fbeba",
                "name": "Netflix",
                "description": "A good way to watch and enjoy your time",
                "price": 20,
                "quantity": 10,
                "categories": "Software",
                "isActive": true,
                "dateAdded": "2022-06-20T06:26:31.952Z",
                "_id": "62b013436bc5200cc996f3ff",
                "customers": [],
                "review": [],
                "__v": 0
            }
            
            */
            let a = {
              productId: data._id,
            };

            result.products.push(a);

            // result.products.push(data._id);
            return result
              .save()
              .then(() => true)
              .catch((err) => res.send(err));
          });

          if (checker) {
            res.send(data);
          }
        })
        .catch((err) =>
          res.send({
            message: err.message,
            choices: err.errors.categories.properties.enumValues,
          })
        );
    }
  });
};

//edit product
const editProducts = (req, res) => {
  let { name, description, price, quantity, categories } = req.body;

  let editProducts = {
    name,
    description,
    price,
    quantity,
    categories,
  };

  //   Seller.findOne({ name }).then((data) => {
  //     if (data !== null && data.name === name) {
  //       return res.send("Product already exists");
  //     } else {
  Product.findByIdAndUpdate(req.params.id, editProducts, { new: true })
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};
//   });
// };

//delete/archive product
const deleteProduct = (req, res) => {
  let deleteItem = {
    isActive: false,
  };

  Product.findByIdAndUpdate(req.params.id, deleteItem, { new: true })
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};

//view all products (active only)
const viewAllProductsSeller = (req, res) => {
  Product.find({})
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};

//AISHA
let sellerRegister = (req, res) => {
  const { name, description, email, password } = req.body;

  Seller.countDocuments({ name }, (err, count) => {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);

    if (count === 0) {
      let newSeller = new Seller({
        name,
        description,
        email,
        password: hashedPassword,
      });

      newSeller
        .save()
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
    }
  });
};

//view single product
const viewSeller = (req, res) => {
  Seller.findById(req.params.id)
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};

module.exports = {
  addProducts,
  editProducts,
  deleteProduct,
  viewAllProductsSeller,
  sellerRegister,
  viewSeller,
};
